package example.pokemon.example_lib.shared.domain.stock

import example.pokemon.example_lib.shared.data.stock.network.response.StockResponse
import example.pokemon.example_lib.shared.domain.stock.entity.Stock

interface StockRepository {
    suspend fun getStocks(limit: Int, page: Int): StockResponse?
    suspend fun getStocksLocal(offset: Int, limit: Int): List<Stock>
    suspend fun setStocksLocal(stocks: MutableList<Stock>)
}