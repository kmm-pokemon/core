package example.pokemon.example_lib.shared.app.stockedit

import example.pokemon.core.shared.app.common.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow

@Suppress("kotlin:S6305")
class StockEditViewModel : BaseViewModel() {
    val field1 = MutableStateFlow<String?>(null)
    val field1Error = MutableStateFlow<String?>(null)
    val field2 = MutableStateFlow<String?>(null)
    val field2Error = MutableStateFlow<String?>(null)
    val field3 = MutableStateFlow<String?>(null)
    val field3Error = MutableStateFlow<String?>(null)
    val field4 = MutableStateFlow<String?>(null)
    val field4Error = MutableStateFlow<String?>(null)
}