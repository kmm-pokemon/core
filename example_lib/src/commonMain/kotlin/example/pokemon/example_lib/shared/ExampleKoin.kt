package example.pokemon.example_lib.shared

import example.pokemon.core.shared.Context
import example.pokemon.core.shared.initKoin
import example.pokemon.example_lib.shared.domain.stock.entity.Stock
import example.pokemon.example_lib.shared.domain.stock.usecase.*
import io.ktor.http.*
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration

fun protocolShared() = URLProtocol.HTTPS
fun prefsNameShared() = "pri0r_m1cr0_h1ght_c0r3"
fun provideStockDb(): Realm {
    val config = RealmConfiguration.Builder(
        schema = setOf(Stock::class)
    ).build()
    return Realm.open(config)
}

@Suppress("UnUsed")
fun initKoin(
    context: Context?,
    host: String,
    deviceId: String,
    version: String,
) = initKoin(context, host, protocolShared(), prefsNameShared(), deviceId, version) {
    modules(libModule())
}