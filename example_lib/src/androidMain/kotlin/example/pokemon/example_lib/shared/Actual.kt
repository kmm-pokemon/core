package example.pokemon.example_lib.shared

import example.pokemon.example_lib.shared.app.onboarding.OnBoardingViewModel
import example.pokemon.example_lib.shared.app.stockdetailsheet.StockDetailSheetViewModel
import example.pokemon.example_lib.shared.app.stockedit.StockEditViewModel
import example.pokemon.example_lib.shared.app.stocklist.StockListViewModel
import example.pokemon.example_lib.shared.data.stock.StockRepositoryImpl
import example.pokemon.example_lib.shared.domain.stock.StockRepository
import example.pokemon.example_lib.shared.domain.stock.usecase.GetStocksLocalUseCase
import example.pokemon.example_lib.shared.domain.stock.usecase.GetStocksUseCase
import example.pokemon.example_lib.shared.domain.stock.usecase.SetStocksLocalUseCase
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

actual fun libModule() = module {
    viewModelOf(::OnBoardingViewModel)
    singleOf(::provideStockDb)

    single<StockRepository> { StockRepositoryImpl() }
    singleOf(::GetStocksUseCase)
    singleOf(::GetStocksLocalUseCase)
    singleOf(::SetStocksLocalUseCase)

    viewModelOf(::StockListViewModel)
    viewModelOf(::StockDetailSheetViewModel)
    viewModelOf(::StockEditViewModel)
}