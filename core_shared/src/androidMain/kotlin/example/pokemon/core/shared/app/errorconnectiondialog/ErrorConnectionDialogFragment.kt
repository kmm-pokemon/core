package example.pokemon.core.shared.app.errorconnectiondialog

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import example.pokemon.core.shared.R
import example.pokemon.core.shared.app.common.BaseDialogFragment
import example.pokemon.core.shared.databinding.ErrorConnectionDialogFragmentBinding
import example.pokemon.core.shared.external.constant.AppConstant


class ErrorConnectionDialogFragment :
    BaseDialogFragment<ErrorConnectionDialogFragmentBinding>(R.layout.error_connection_dialog_fragment) {
    override fun showCloseButton() = false
    override fun isCancelable() = false
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mbErrorConnectionRetry.setOnClickListener {
            setFragmentResult(
                AppConstant.RETRY_KEY,
                bundleOf(AppConstant.RETRY_KEY to arguments?.getString("key", null))
            )
            dismiss()
        }
    }
}