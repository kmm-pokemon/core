package example.pokemon.core.shared.app.splash

import android.os.Bundle
import android.view.View
import example.pokemon.core.shared.R
import example.pokemon.core.shared.app.common.BaseFragment
import example.pokemon.core.shared.databinding.SplashFragmentBinding
import example.pokemon.core.shared.external.extension.loadImage

class SplashFragment : BaseFragment<SplashFragmentBinding>(R.layout.splash_fragment) {
    override fun showActionBar() = false
    override fun isFullScreen() = true

    @Suppress("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ivSplash.loadImage(R.drawable.pokeball)
    }
}