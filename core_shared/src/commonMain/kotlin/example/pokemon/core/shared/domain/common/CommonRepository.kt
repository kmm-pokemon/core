package example.pokemon.core.shared.domain.common

import example.pokemon.core.shared.data.common.network.response.ApiResponse
import example.pokemon.core.shared.domain.common.entity.Ticket

interface CommonRepository {
    suspend fun refreshToken(refreshToken: String, phone: String): ApiResponse<Ticket?>?
}