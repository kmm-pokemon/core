package example.pokemon.core.shared.data.common

import example.pokemon.core.shared.data.common.network.request.PhoneReq
import example.pokemon.core.shared.data.common.network.response.ApiResponse
import example.pokemon.core.shared.domain.common.CommonRepository
import example.pokemon.core.shared.domain.common.entity.Ticket
import example.pokemon.core.shared.external.utility.ApiClient
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*

class CommonRepositoryImpl(
    private val apiClient: ApiClient,
) : CommonRepository {
    override suspend fun refreshToken(refreshToken: String, phone: String): ApiResponse<Ticket?>? =
        apiClient.client.post("/api/traveller/v1/token") {
            headers.append("x-client-refresh-token", refreshToken)
            contentType(ContentType.Application.Json)
            setBody(PhoneReq(phone.replace("+", "")))
        }.body()
}