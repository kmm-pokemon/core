package example.pokemon.core.shared.external.enum

enum class ErrorEnum {
    AccountLocked,
    AuthError,
    DataNotFound,
    TokenError,
    TooManyRetryOTPLogin,
    TooManyRetryOTPRegistration,
    TooManyRetryEmailVerification,
    TooManyChangeEmailVerification,
    RequestOTPErrorLogin,
    RequestOTPErrorRegistration,
}