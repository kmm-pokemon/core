package example.pokemon.core.shared.data.common.network.response

import example.pokemon.core.shared.domain.common.entity.Meta
import kotlinx.serialization.Serializable

@Suppress("Unused")
@Serializable
data class ApiResponse<D>(
    var meta: Meta? = null,
    var data: D? = null,
)