@file:Suppress("Unused")

package example.pokemon.core.shared.external.constant

object AppConstant {
    // Default Values
    const val CHANNEL = "APP"
    const val LIST_LIMIT = 21
    const val CURRENCY = "Rp "
    const val LOADING = "loading..."

    // Key List
    const val MULTIPLE_KEY = "MULTIPLE"
    const val EVOLVING_KEY = "EVOLVING"
    const val ACCESS_TOKEN_KEY = "ACCESS_TOKEN"
    const val REFRESH_TOKEN_KEY = "REFRESH_TOKEN"
    const val PHONE_KEY = "PHONE"
    const val FILTERED_KEY = "FILTERED"
    const val DELETED_KEY = "DELETED"
    const val SELECT_DATE_KEY = "SELECT_DATE"
    const val ONBOARDING_KEY = "ONBOARDING"
    const val PAYMENT_KEY = "payment"
    const val RETRY_KEY = "retry"

    // Format
    const val NORMAL_DATE_FORMAT = "dd-MM-yyyy"
    const val API_DATE_FORMAT = "yyyy-MM-dd"
}