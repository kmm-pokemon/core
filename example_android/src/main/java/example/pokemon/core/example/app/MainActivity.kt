package example.pokemon.core.example.app

import example.pokemon.core.example.R
import example.pokemon.core.shared.app.common.BaseActivity
import example.pokemon.example_lib.shared.R as eR

class MainActivity : BaseActivity() {
    override fun appVersion() = getString(R.string.app_version)
    override fun navGraph() = R.navigation.main_nav_graph
    override fun topLevelDestinations(): Set<Int> {
        val list = HashSet<Int>()
        list.add(eR.id.exploreFragment)
        list.add(eR.id.collectionsFragment)
        list.add(eR.id.stockDetailSheetFgragment)
        return list
    }
}