import Foundation
import UIKit

public class BottomNavController: UITabBarController {
    public override func viewDidLoad() {
        super.viewDidLoad()
        viewControllers?[0].tabBarItem = UITabBarItem(title: "Explore", image: UIImage(systemName: "magnifyingglass"), tag: 0)
        viewControllers?[1].tabBarItem = UITabBarItem(title: "Collection", image: UIImage(systemName: "bookmark.square"), tag: 1)
        
        if #available(iOS 13, *) {
            let appearance = tabBar.standardAppearance.copy()
            appearance.backgroundImage = UIImage()
            appearance.shadowImage = UIImage()
            tabBar.standardAppearance = appearance
        } else {
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
        }

        tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        tabBar.layer.shadowOpacity = 0.3
        tabBar.layer.shadowRadius = 5
        tabBar.tintColor = .primary
    }
}
