import core_shared
import SDWebImage

class MainNavController: UIViewController {
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var logoImage: SDAnimatedImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        versionLabel.text = "Versi \(AppInfo.appVersion)"
        versionLabel.isHidden = true
        logoImage.image = SDAnimatedImage(named: "pokeball.gif")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let skipedOnBoarding = Preferences.value(forKey: AppConstant.shared.ONBOARDING_KEY, defaultValue: false)
            if skipedOnBoarding {
                let isMultiple = Preferences.value(forKey: AppConstant.shared.MULTIPLE_KEY, defaultValue: false)
                if isMultiple {
                    self.performSegue(withIdentifier: "MainToBottomNav", sender: nil)
                } else {
                    let isEvolving = Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, defaultValue: "")
                    self.performSegue(withIdentifier: isEvolving.isEmpty ? "MainToHomeNav" : "MainToDetailNav", sender: nil)
                }
            } else {
                self.performSegue(withIdentifier: "MainToOnBoardingNav", sender: nil)
            }
         }
    }
}
